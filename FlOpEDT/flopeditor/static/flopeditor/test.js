var year = 2018 ;
var week = 38 ;
var cours ;

$.ajax({
    type: "GET", //rest Type
    dataType: 'text',
    url: url_cours + year + "/" + week ,
    async: true,
    contentType: "text/csv",
    success: function(msg, ts, req) {
        console.log(msg);

        cours = d3.csvParse(msg, translate_cours_from_csv);
	console.log(cours);
    },
    error: function(msg) {
        console.log("error");
    }
});


function translate_cours_from_csv(d) {
    var co = {
        id_cours: +d.id,
        no_cours: +d.no,
        prof: d.prof,
        group: d.groupe,
        promo: d.promo,
        mod: d.module,
	color_bg: d.color_bg,
	color_txt: d.color_txt,
    };
    return co ;
}
