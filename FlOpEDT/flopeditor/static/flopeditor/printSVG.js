

function mouse_handler(){
  course_list = [];
  $("rect").click(function(d){
    if(typeof(d.target.__data__.selected) == 'undefined'){
      d.target.__data__.selected = true;
      $(this).attr("opacity", 0.5);
      course_list.push(d.target.__data__["id_cours"]);
    } else {
      if(d.target.__data__.selected){
        d.target.__data__.selected = false;
        $(this).attr("opacity", 1);
        course_list.splice(course_list.indexOf(d.target.__data__["id_cours"]), 1);
      } else {
        d.target.__data__.selected = true;
        $(this).attr("opacity", 0.5);
        course_list.push(d.target.__data__["id_cours"]);
      }
    }

    course_list_tostring = "";

    course_list.forEach(function(d, i){
      if(i == 0){
        course_list_tostring += "" + d;
      } else {
        course_list_tostring += "," + d;
      }
    });

    $("#course_list").attr("value", course_list_tostring);

    console.log($(this));
  });
}

function create_svg_week(dataset){

  var classes = {
    "INFO1":{
      "CE":"CE",
      "TD":RegExp("^[1-4]$"),
      "TP":RegExp("^[1-4][AB]$")
    },
    "INFO2":{
      "CE":"CE",
      "TD":RegExp("^[1-3]$"),
      "TP":RegExp("^[1-3][AB]$")
    }
  };

  var tabCours = {
    "INFO1":{
      "CE":[],
      "TD":[],
      "TP":[]
    },
    "INFO2":{
      "CE":[],
      "TD":[],
      "TP":[]
    }
  };

  dataset.forEach(function(d, i){
    if(classes[d["promo"]]["CE"] == d["group"]){
      tabCours[d["promo"]]["CE"].push(d);
    }
    if(classes[d["promo"]]["TD"].test(d["group"])){
      tabCours[d["promo"]]["TD"].push(d)
    }
    if(classes[d["promo"]]["TP"].test(d["group"])){
      tabCours[d["promo"]]["TP"].push(d)
    }
  });
  // Largeur et hauteur
  var w = 800;  // largeur
  var h = 1000;  // hauteur
  var hCours = 50;
  var largeurCol = w/4;
  var espace = w/20;

  /*var wJ = 100; //Largeur en pixel de 1 jour
  var hJ =*/

  //Taille par défaut des cases:
  var hC = 100;   //La même hauteur pour toutes les cases de cour

  var svg = d3.select("body")
              .append("svg")
              .attr("width", "85%")
              .attr("height", 600);

  var svgA = svg.append("svg")
                 .attr("width", "27%")
                 .attr("x", "4%");

  var svgTD = svg.append("svg")
                 .attr("width", "27%")
                 .attr("x", (27+4*2+"%"));

  var svgTP = svg.append("svg")
                 .attr("width", "27%")
                 .attr("x", ((27*2)+(4*3)+"%"));

  svgA.append("text")
       .attr("x", "50%")
       .attr("y", "5%")
       .text("Amphi")
       .attr("text-anchor", "middle");

  svgTD.append("text")
       .attr("x", "50%")
       .attr("y", "5%")
       .text("TD")
       .attr("text-anchor", "middle");

  svgTP.append("text")
       .attr("x", "50%")
       .attr("y", "5%")
       .text("TP")
       .attr("text-anchor", "middle");

  var svgCoursA = svgA.append("svg")
                      .attr("width", "100%")
                      .attr("height", "91%")
                      .attr("y", "9%");
  var svgCoursTD = svgTD.append("svg")
                      .attr("width", "100%")
                      .attr("height", "91%")
                      .attr("y", "9%");
  var svgCoursTP = svgTP.append("svg")
                      .attr("width", "100%")
                      .attr("height", "91%")
                      .attr("y", "9%");


  svgCoursA.selectAll("rect")
      .data(tabCours["INFO1"]["CE"])
      .enter()
      .append("rect")
      .attr("y", function(d, i){
        return (i)*hCours + 5;
      })
      .attr("x", 1)
      .attr("height", hCours)
      .attr("width", "98%")
      .attr("fill", function(d){
        return d["color_bg"];
      })
      .attr("stroke", "black")
      .attr("stroke-width", 2)
      .attr("id", function(d){
        return d["id_cours"];
      });


  svgCoursTD.selectAll("rect")
      .data(tabCours["INFO1"]["TD"])
      .enter()
      .append("rect")
      .attr("y", function(d, i){
        return (parseInt(i/4))*hCours + 5;
      })
      .attr("x", function(d, i){
        return (i%4)*25 + "%";
      })
      .attr("height", hCours)
      .attr("width", "25%")
      .attr("fill", function(d){
        return d["color_bg"];
      })
      .attr("stroke", "black")
      .attr("stroke-width", 2)
      .attr("id", function(d){
        return d["id_cours"];
      });


  svgCoursTP.selectAll("rect")
      .data(tabCours["INFO1"]["TP"])
      .enter()
      .append("rect")
      .attr("y", function(d, i){
        return (parseInt(i/8))*hCours + 5;
      })
      .attr("x", function(d, i){
        return (i%8)*12.5 + "%";
      })
      .attr("height", hCours)
      .attr("width", "12.5%")
      .attr("fill", function(d){
        return d["color_bg"];
      })
      .attr("stroke", "black")
      .attr("stroke-width", 2)
      .attr("id", function(d){
        return d["id_cours"];
      });


  tabCours["INFO1"]["CE"].forEach(function(d, i){
    svgCoursA.append("text")
            .attr("y", function(){
              return (i + 0.40)*hCours;
            })
            .attr("x", "50%")
            .text(function(){
              return d["mod"];
            })
            .attr("fill", function(){
              return d["color_txt"];
            })
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("id", function(){
              return d["id_cours"];
            });

    svgCoursA.append("text")
            .attr("y", function(){
              return (i + 0.70)*hCours;
            })
            .attr("x", "50%")
            .text(function(){
              return d["prof"];
            })
            .attr("fill", function(){
              return d["color_txt"];
            })
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("id", function(){
              return d["id_cours"];
            });
  })

  tabCours["INFO1"]["TD"].forEach(function(d, i){
    svgCoursTD.append("text")
            .attr("y", function(){
              return (parseInt(i/4) + 0.32)*hCours;
            })
            .attr("x", function(){
              return (i%4 + 0.5)*25 + "%";
            })
            .text(function(){
              return d["mod"];
            })
            .attr("fill", function(){
              return d["color_txt"];
            })
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("id", function(){
              return d["id_cours"];
            });

    svgCoursTD.append("text")
            .attr("y", function(){
              return (parseInt(i/4) + 0.60)*hCours;
            })
            .attr("x", function(){
              return (i%4 + 0.5)*25 + "%";
            })
            .text(function(){
              return d["prof"];
            })
            .attr("fill", function(){
              return d["color_txt"];
            })
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("id", function(){
              return d["id_cours"];
            });

    svgCoursTD.append("text")
            .attr("y", function(){
              return (parseInt(i/4) + 0.87)*hCours;
            })
            .attr("x", function(){
              return (i%4 + 0.5)*25 + "%";
            })
            .text(function(){
              return d["group"];
            })
            .attr("fill", function(){
              return d["color_txt"];
            })
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("id", function(){
              return d["id_cours"];
            });
  })

  tabCours["INFO1"]["TP"].forEach(function(d, i){
    svgCoursTP.append("text")
            .attr("y", function(){
              return (parseInt(i/8) + 0.32)*hCours;
            })
            .attr("x", function(){
              return (i%8 + 0.5)*12.5 + "%";
            })
            .text(function(){
              return d["mod"];
            })
            .attr("fill", function(){
              return d["color_txt"];
            })
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("id", function(){
              return d["id_cours"];
            });

    svgCoursTP.append("text")
            .attr("y", function(){
              return (parseInt(i/8) + 0.60)*hCours;
            })
            .attr("x", function(){
              return (i%8 + 0.5)*12.5 + "%";
            })
            .text(function(){
              return d["prof"];
            })
            .attr("fill", function(){
              return d["color_txt"];
            })
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("id", function(){
              return d["id_cours"];
            });

    svgCoursTP.append("text")
            .attr("y", function(){
              return (parseInt(i/8) + 0.87)*hCours;
            })
            .attr("x", function(){
              return (i%8 + 0.5)*12.5 + "%";
            })
            .text(function(){
              return d["group"];
            })
            .attr("fill", function(){
              return d["color_txt"];
            })
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .attr("id", function(){
              return d["id_cours"];
            });
  })


}
