from django.shortcuts import render, redirect

from django.http import HttpResponse
from django.http import HttpResponseRedirect

from base.models import Course, Module, Group, TrainingProgramme, CourseType
from people.models import Tutor
from flopeditor.admin import CoursResourceTest
import re


def test(req):
    return render(req, 'flopeditor/test.html')

def editor(req, year, week):
    try:
        week = int(week)
        year = int(year)
    except ValueError:
        return HttpResponse("KO")


    return render(req, "flopeditor/flopeditor.html", {"year": year, "week": week})

def recup_cours(req, year, week):
    try:
        week = int(week)
        year = int(year)
    except ValueError:
        return HttpResponse("KO")

    dataset = CoursResourceTest() \
        .export(Course.objects \
                .filter(semaine=week,
                        an=year))

    return HttpResponse(dataset.csv, content_type='text/csv')

def ajout_cours(req):# module, group, prof, year, week_list
    if req.method != 'POST':
        return HttpResponse("KO")

    try:
        tab_week_str = req.POST["week_list"].split(',')
        tab_week = []
        for week_str in tab_week_str:
            tab_week.append(int(week_str))

        year = int(req.POST["year"])
    except ValueError:
        return HttpResponse("KO")

    mod = Module.objects.filter(abbrev = req.POST["module"]).first()
    prof = Tutor.objects.filter(username = req.POST["prof"]).first()
    group = Group.objects.filter(nom = req.POST["group"], train_prog = mod.train_prog ).first()


    if re.match("^CE$", req.POST["group"]):
        cType = "CM"
    elif re.match("^[1-4]$", req.POST["group"]):
        cType = "TD"
    elif re.match("^[1-4][AB]$", req.POST["group"]):
        cType = "TP"
    else:
        return HttpResponse("KO")

    courseType = CourseType.objects.get(name = cType)

    for week in tab_week:
        course = Course(tutor=prof, groupe=group, module=mod, semaine=week, an=year, type=courseType)
        course.save()

    return redirect("/editor/" + req.POST["year"] + "/" + week_str)

def suppr_cours(req, year, week):
    tab_id_str = req.POST["course_list"].split(',')
    for id_str in tab_id_str:
        Course.objects.get(id = int(id_str)).delete()

    return redirect("/editor/" + year + "/" + week)
